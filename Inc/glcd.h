
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __glcd_H
#define __glcd_H
#ifdef __cplusplus
 extern "C" {
#endif

     
#include "stdint.h"     
/*
 * PIN DEFINES
 */

//Defines del display G240128
#define SIZEPIX_X 240
#define SIZEPIX_Y 128
#define CHAR_SIZEX 	6				//tamaño en x de un caracter

#define LCD_COLS	SIZEPIX_X/CHAR_SIZEX	// columnas de texto del display gráfico
#define LCD_ROWS	SIZEPIX_Y/8				// filas de texto del display
//#define LCD_BASE_GRAPHIC 40*8		// comienzo del area gráfica.


//Ubicacione en la memoria RAM
#define LCD_TEXT_BASE	0x0000
#define LCD_GRAPH_BASE  0x0c00   

     
//register setting
#define LCD_2D_SETCURSORPTR		0x21	//!<setea el puntero del cursor 
#define LCD_2D_SETOFFSETREG		0x22	//!<setea el offset
#define LCD_2D_SETADDRPTR		0x24	//!<setea la dirección del puntero
//set control word
#define LCD_2D_SETTXTHOMEADDR	0x40	//!<setea la dirección home del texto
#define LCD_2D_SETTXTAREA		0x41	//!<setea el area de texto
#define LCD_2D_SETGRPHOMEADDR	0x42	//!<setea la dirección home de graficos
#define LCD_2D_SETGRPAREA		0x43	//!<setea el area de gráficos
//mode set
#define LCD_ORMODE				0x80	//!<modo OR
#define LCD_EXORMODE			0x81	//!<modo EXOR
#define LCD_ANDMODE				0x83	//!<modo AND
#define LCD_TXTATTRMODE			0x84	//!<modo atributo de texto
#define LCD_INTCGROMMODE		0x80	//!<modo internal CGROM 
#define LCD_EXTCGRAMMODE		0x88	//!<modo external CG RAM
//display mode
#define LCD_DISPLAYOFF			0x90	//!<apagar display
#define LCD_CURONBLKOFF			0x92	//!<cursor on, blink off
#define LCD_CURONBLKON			0x93	//!<cursor on, blink on
#define LCD_TXTONGRPOFF			0x94	//!<texto on, graphic off
#define LCD_TXTOFFGRPON			0x98	//!<texto off, graphic on
#define LCD_TXTONGRPON			0x9C	//!<Text on, graphic on
//cursor pattern select
#define LCD_CURPATTR1L			0xa0	//!<cursor de 1 línea
#define LCD_CURPATTR2L			0xa1	//!<cursor de 2 líneas
#define LCD_CURPATTR3L			0xa2	//!<cursor de 3 líneas
#define LCD_CURPATTR4L			0xa3	//!<cursor de 4 líneas
#define LCD_CURPATTR5L			0xa4	//!<cursor de 5 líneas
#define LCD_CURPATTR6L			0xa5	//!<cursor de 6 líneas
#define LCD_CURPATTR7L			0xa6	//!<cursor de 7 líneas
#define LCD_CURPATTR8L			0xa7	//!<cursor de 8 líneas
//data auto read/write		
#define LCD_SETAUTWR			0xB0	//!<Set data auto write
#define LCD_SETAUTRD			0xB1	//!<Set data auto read
#define LCD_AUTORESET			0xB2	//!<auto reset
//data read/write
#define LCD_1D_WRDATAINC		0xC0	//!<Escribe dato en incrementa ADP
#define LCD_RDDATAINC			0xC1	//!<Escribe dato en incrementa ADP
#define LCD_1D_WRDATADEC		0xC2	//!<Escribe dato en incrementa ADP
#define LCD_RDDATADEC			0xC3	//!<Escribe dato en incrementa ADP
#define LCD_1D_WRDATA			0xC4	//!<Escribe dato sin variar ADP 
#define LCD_RDDATA				0xC5	//!<Lee dato sin variar ADP
//Screen 
#define LCD_SCRPEEK				0xE0	//!<Screen peek
#define LCD_SCRCP				0xE8	//!<Screen copy
//bit set/reset		
#define LCD_BCLR				0xF0	//!<Bit Reset	
#define LCD_BSET				0xF8	//!<Bit set
#define LCD_B0					0xF1	//!<Bit 0 (LSB)
#define LCD_B1					0xF2	//!<Bit 1 
#define LCD_B2					0xF3	//!<Bit 2 
#define LCD_B3					0xF4	//!<Bit 3 
#define LCD_B4					0xF5	//!<Bit 4 
#define LCD_B5					0xF6	//!<Bit 5 
#define LCD_B6					0xF7	//!<Bit 6 
#define LCD_B7					0xF8	//!<Bit 7 (MSB)
     

/**
 * Variable que toma el estado del LCD
 */
typedef union _LCD_STATUS_REG_TYPE {
	uint8_t BYTE; //!< byte con del dato registro LCD
	struct {
		uint8_t STA0 :1;
		uint8_t STA1 :1;
		uint8_t STA2 :1;
		uint8_t STA3 :1;
		uint8_t STA4 :1;
		uint8_t STA5 :1;
		uint8_t STA6 :1;
		uint8_t STA7 :1;
	} Bits; //!< variable tipo struct para poder acceder bit a bit
} LcdStatusReg_t;


typedef enum{
	ImpNormal,	 
	Imp90,		// imprimir a 90 grados
	Imp180,		// 180 grados
	Imp270,		// 270 grados
	ImpFH,		// flip horizontal
	ImpFV		// flip vertical
} modoImpresion_t;
/*
 * Functions
 */

void lcdInit(void);
void lcdClearText(void);
void lcdClearGraphics(void);

void scrollText(uint8_t scroll);

//verificación del estado del LCD
// funciones locales de glcd.c
static volatile void lcdReadStatus(void);
static uint8_t lcdReady(void);
static uint8_t lcdAutoWRReady(void);
static uint8_t lcdAutoRDReady(void);
static void lcdSendData(uint8_t);
static void lcdSendAutoData(uint8_t);
static void lcdSendCmd(uint8_t);
static void lcdSend2DCmd(uint16_t dato, uint8_t cmd);
static void lcdCmdSetCursorPointer(uint8_t, uint8_t);
static void lcdCmdSetOffsetRegister(uint8_t);
static void lcdCmdSetAddressPointer(uint8_t, uint8_t);

void lcdOffGraph(void);
void lcdOnGraphAndText(void);

//Funciones de dibujo:
void lcdDrawBitMap(uint8_t x, uint8_t y, uint8_t* bitmap, modoImpresion_t modo);
void lcdDrawBox(uint8_t x, uint8_t y, uint8_t sizex, uint8_t sizey, uint8_t color);
void lcdDrawPixel(uint16_t , uint16_t, uint8_t);
void lcdPutString(uint8_t*, uint8_t, uint8_t);
void lcdDrawLine(int16_t X1, int16_t Y1,int16_t X2,int16_t Y2, int8_t color);
void lcdCircle(uint8_t, uint8_t,uint8_t, uint8_t);
void lcdDrawRectangle(uint8_t, uint8_t, uint8_t, uint8_t, uint8_t);

#endif
