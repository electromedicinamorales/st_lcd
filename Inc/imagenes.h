/**
 * @file imagenes.h
 *
 * @date 13-abr-2016
 * @author Santiago Paleka
 */

#ifndef IMAGENES_H_
#define IMAGENES_H_

#include "stm32f0xx_hal.h"

extern const uint8_t cteLogo[];
extern const uint8_t cteFlecha[];

#endif /* IMAGENES_H_ */
