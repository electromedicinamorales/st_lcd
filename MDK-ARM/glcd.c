/**
 * 
 * 
 *  PINES LCD PUERTOS
 *        D0 = PB6  ENTRADA/SALIDA NECESITAN SER FIVE TOLERANT
 *        D1 = PC7  ENTRADA/SALIDA NECESITAN SER FIVE TOLERANT
 *        D2 = PA9  ENTRADA/SALIDA NECESITAN SER FIVE TOLERANT
 *        D3 = PA8  ENTRADA/SALIDA NECESITAN SER FIVE TOLERANT
 *        D4 = PB10 ENTRADA/SALIDA NECESITAN SER FIVE TOLERANT 
 *        D5 = PB4  ENTRADA/SALIDA NECESITAN SER FIVE TOLERANT
 *        D6 = PB5  ENTRADA/SALIDA NECESITAN SER FIVE TOLERANT
 *        D7 = PB3  ENTRADA/SALIDA NECESITAN SER FIVE TOLERANT
 *        CE = PA5  SIEMPRE SALIDA
 *        WR = PB8  SIEMPRE SALIDA
 *        RD = PB9  SIEMPRE SALIDA
 *        CD = PA6  SIEMPRE SALIDA
 *        FS = PA10 SIEMPRE SALIDA
*/


/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"
#include "glcd.h"
#include "gpio.h"
#include "tim.h"
#include "imagenes.h"

/* Private variables ---------------------------------------------------------*/

volatile LcdStatusReg_t lcdStatusReg;
/* Extern variables ----------------------------------------------------------*/
extern volatile uint32_t delayms;
/* Private function prototypes -----------------------------------------------*/
/**
* Resetea DISPLAY
*/
void lcd_HoldReset(void){
    LCD_FS_GPIO_Port->BSRR = LCD_FS_Pin;    // ancho 6 puntos
	LCD_WR_GPIO_Port->BSRR = LCD_WR_Pin;    // Deshabilito escritura,
	LCD_RD_GPIO_Port->BRR = LCD_RD_Pin;     // Deshabilito Lectura.
	LCD_CD_GPIO_Port->BRR = LCD_CD_Pin;     // CD = 0
	LCD_RST_GPIO_Port->BRR = LCD_RST_Pin;   // RST = 0	
	LCD_CE_GPIO_Port->BRR = LCD_CE_Pin;	    // CE = 0	
}

/**
* Deshabilita el LCD.
*/
void lcdDisable(void){
	LCD_CE_GPIO_Port->BSRR = LCD_CE_Pin;	// CE = 1
	LCD_WR_GPIO_Port->BSRR = LCD_WR_Pin;   // Deshabilito escritura,
	LCD_RD_GPIO_Port->BSRR = LCD_RD_Pin;   // Deshabilito Lectura.
}

/**
*   Lee lo que hay actualmente en la salida del puerto LCD y lo devuelve en un uint8_t
* Se hace mediante una función por q lamentablemente los pines están desordenados y en 
* varios puertos no consecutivos.
* 
*  PINES LCD PUERTOS
*       D0 = PB6
*       D1 = PC7
*       D2 = PA9
*       D3 = PA8
*       D4 = PB10
*       D5 = PB4
*       D6 = PB5
*       D7 = PB3
*/
uint8_t lcdReadPort(void){
    uint8_t temp=0;
    uint16_t inp;
    inp = GPIOA->IDR;
    temp |= inp&1<<9 ? 1<<2 : 0;
    temp |= inp&1<<8 ? 1<<3 : 0;
    inp = GPIOB->IDR;
    temp |= inp&1<<6 ? 1<<0 : 0;
    temp |= inp&1<<10? 1<<4 : 0;
    temp |= inp&1<<4 ? 1<<5 : 0;
    temp |= inp&1<<5 ? 1<<6 : 0;
    temp |= inp&1<<3 ? 1<<7 : 0;
    inp = GPIOC->IDR;
    temp |= inp&1<<7 ? 1<<1 : 0;
    return temp;
}

/**
*   Escribe en la salida del puerto LCD 
* Se hace mediante una función por q lamentablemente los pines están desordenados y en 
* varios puertos no consecutivos.
* 
*  PINES LCD PUERTOS
*       D0 = PB6
*       D1 = PC7
*       D2 = PA9
*       D3 = PA8
*       D4 = PB10
*       D5 = PB4
*       D6 = PB5
*       D7 = PB3
*/
void lcdWritePort(uint8_t dato){
    uint32_t temp=0;   
    //temp |= dato&1<<2 ? 1<<9:0;
    if((dato & 0x4)!=0)
        temp = 0x200;
    //temp |= dato&1<<3 ? 1<<8:0;
    if((dato & 0x08)!=0)
        temp|= 0x100;
    GPIOA->BRR = 1<<8 | 1<<9; //bits a 0
    GPIOA->BSRR = temp; //escribe los dos bits del puerto A D2 y D3
    temp=0;
    //temp |= dato&1<<0 ? 1<<6 : 0;
    if((dato & 0x01)!=0)
        temp|= 0x40;
    //temp |= dato&1<<4 ? 1<<10: 0;
    if((dato & 0x10)!=0)
        temp|= 0x400;
    //temp |= dato&1<<5 ? 1<<4 : 0;
    if((dato & 0x20)!=0)
        temp|= 0x10;
    //temp |= dato&1<<6 ? 1<<5 : 0;
    if((dato & 0x40)!=0)
        temp|= 0x20;
    //temp |= dato&1<<7 ? 1<<3 : 0;
    if((dato & 0x80)!=0)
        temp|= 0x08;
    GPIOB->BRR = 1<<6 | 1<<10 | 1<<4 | 1<<5 | 1<<3 ; //bits a 0
    GPIOB->BSRR = temp; //escribe los dos bits del puerto B6 B10 B4 B5 B3
    temp=0;
    //temp |= dato&1<<1 ? 1<<7 : 0;
    if((dato & 0x02)!=0)
        temp|= 0x80;
    GPIOC->BRR = 1<<7;
    GPIOC->BSRR = temp; //escribe los dos bits del puerto C7
}




/**
 * Lee el estado del LCD
 * \param[out] lcdStatusReg con el estado actual del registro.
 */
static volatile void lcdReadStatus(void) {
    //uint8_t i;
	// Pongo todos los pines del LCD_DATO como entrada
    GPIOC->MODER &= ~(0x3<<(7*2)); // GPIO_C7 como entrada 
    GPIOB->MODER &= ~(0x3<<(3*2) | 0x3<<(4*2) | 0x3<<(5*2) |0x3<<(6*2) | 0x3<<(10*2)) ; // GPIOs B3 B5 B6 y B10 como entrada 
    GPIOA->MODER &= ~(0x3<<(8*2) | 0x3<<(9*2)); // GPIO_A 8 y 9 como entrada 
   
    /* Bits de control configurados como lectura de comando*/
    LCD_CD_GPIO_Port->BSRR = LCD_CD_Pin;
    LCD_WR_GPIO_Port->BSRR = LCD_WR_Pin; // PIN WR EN 1
    LCD_RD_GPIO_Port->BRR  = LCD_RD_Pin; // PIN RD EN 0
    //for (i=0;i<10;i++){
        __NOP();
    //}
    LCD_CE_GPIO_Port->BRR  = LCD_CE_Pin; // CE=0
    //for (i=0;i<10;i++){
        __NOP();
    //}
   /* Leo el puerto */
	lcdStatusReg.BYTE = lcdReadPort(); // Lee el puerto.
    lcdDisable();
}


/**
 * Función que verifica la disponibilidad de enviar o leer un comando/dato
 * Esta función se bloquea hasta que delayms
 * lcdReady es la función que genera un error del tipo RET_LCD_FAIL que se
 * propagará hasta el primer llamado a una funcion lcdXXXX 
 */
uint8_t lcdReady(void){
	delayms = 100;
    lcdStatusReg.BYTE = 0;
    while ((lcdStatusReg.Bits.STA0 == 0) && (lcdStatusReg.Bits.STA1 == 0)) { //!<Verificar estado del display
		lcdReadStatus();
		if(delayms == 0) { // salir si no se inicializa
			return 1;
		}
	}
	return 0;
}

/**
 * Función de inicialización del LCD.
 * Inicializa los puertos que van a usarse en el LCD\n
 * Inicializa el LCD. 
 */
void lcdInit(void){
    uint32_t i;
    uint8_t cadena[] = "PRUEBA DE TEXTO";
	lcd_HoldReset();
    delayms = 10;
    while(delayms);
    LCD_RST_GPIO_Port->BSRR = LCD_RST_Pin;
	LCD_CE_GPIO_Port->BSRR = LCD_CE_Pin;
    if(lcdReady()){
        Error_Handler();
    }
    lcdSend2DCmd( 0x0, LCD_2D_SETTXTHOMEADDR); // texte area 0x00
	lcdSend2DCmd( LCD_GRAPH_BASE, LCD_2D_SETGRPHOMEADDR); //direcciono graficos a 0x0c00
	lcdSend2DCmd(LCD_COLS, LCD_2D_SETGRPAREA);
	lcdSend2DCmd(LCD_COLS, LCD_2D_SETTXTAREA);
	lcdSendCmd(LCD_ORMODE);
	lcdSendCmd(LCD_DISPLAYOFF);
	lcdClearText();
	lcdClearGraphics();
	lcdDrawBitMap(255,24,(uint8_t *)cteLogo, ImpNormal);
	lcdSendCmd(LCD_TXTONGRPON);
}

/**
 * Envia comando con dos datos
 * @param[in] dato dos bytes a enviar antes del comando LSB primero
 * @param[in] cmd comando a enviar, debe ser LCD_2D_*
 * @return None
 */
static void lcdSend2DCmd(uint16_t dato, uint8_t cmd){
	lcdSendData((uint8_t)(dato&0xFF));
	lcdSendData((uint8_t)((dato&0xFF00)>>8));
	lcdSendCmd(cmd);
}

/**
 * Función para enviar comando al lcd
 * Esta función envía un comando al lcd, 
 */
static void lcdSendCmd(uint8_t cmd) {
    //uint8_t i;
	if(lcdReady()){
		__NOP();
		return;
	}
    // Pongo todos los pines del LCD_DATO como salida
    //primero los pongo como entrada, para poner en 00 los dos bits de configuracion.
    GPIOC->MODER &= ~(0x3<<(7*2)); // GPIO_C7 como entrada 
    GPIOB->MODER &= ~(0x3<<(3*2) | 0x3<<(4*2) | 0x3<<(5*2) | 0x3<<(6*2) | 0x3<<(10*2)) ; // GPIOs B3 B5 B6 y B10 como entrada 
    GPIOA->MODER &= ~(0x3<<(8*2) | 0x3<<(9*2)) ; // GPIO_C7 como entrada 
    // para poner como salida, se pone un 01 en el moder correspondiente al pin.
    GPIOC->MODER |= (0x1<<(7*2)); // GPIO_C7 como salida
    GPIOB->MODER |= (0x1<<(3*2) | 0x1<<(4*2) | 0x1<<(5*2) | 0x1<<(6*2) | 0x1<<(10*2)) ; // GPIOs B3 B5 B6 y B10 como salida
    GPIOA->MODER |= (0x1<<(8*2) | 0x1<<(9*2)) ; // GPIO_C7 como salida
    lcdWritePort(cmd);
    LCD_RD_GPIO_Port->BSRR = LCD_RD_Pin; // pin RD en 1
    LCD_WR_GPIO_Port->BRR = LCD_WR_Pin; // pin WR en 0
    LCD_CD_GPIO_Port->BSRR = LCD_CD_Pin; // pin CD en 1
   // for(i=0;i<10;i++){
        __NOP();    
    //}
    LCD_CE_GPIO_Port->BRR = LCD_CE_Pin; // CE = 0
	//for(i=0;i<10;i++){
        __NOP();    
    //}
    lcdDisable();
    GPIOC->MODER &= ~(0x3<<(7*2)); // GPIO_C7 como entrada 
    GPIOB->MODER &= ~(0x3<<(3*2) | 0x3<<(4*2) | 0x3<<(5*2) | 0x3<<(6*2) | 0x3<<(10*2)) ; // GPIOs B3 B5 B6 y B10 como entrada 
    GPIOA->MODER &= ~(0x3<<(8*2) | 0x3<<(9*2)); // GPIO_C7 como entrada 
}

/**
 * Función que verifica la disponibilidad de escribir un dato en modo auto write
 * Esta función se bloquea hasta que delayms = 0 o que se cumpla con READY 
 */
static uint8_t lcdAutoWRReady(void){
	delayms = DELAY_10MS;
	lcdStatusReg.BYTE = 0;
	while (lcdStatusReg.Bits.STA3 == 0) { //Verifica estado del display
		lcdReadStatus();
		if(delayms == 0) { // salir con error si no se inicia el LCD
			return 1; 
		}
	}
	return 0;
}

/**
 * Función que verifica la disponibilidad de leer un dato en modo auto read
 * Esta función se bloquea hasta que delayms = 0 o que se cumpla con READY 
 */
static uint8_t lcdAutoRDReady(void) {
	delayms = DELAY_10MS;
	lcdStatusReg.BYTE = 0;
	while (lcdStatusReg.Bits.STA2 == 0) { //Verifica estado del display
		lcdReadStatus();
		if(delayms == 0) { // salir con error si no se inicia el LCD
			return 1;
		}
	}
	return 0;
}

/*
 * Escribe texto en el display
 * \param[in] text puntero al array de texto, debe terminar con 0
 * \param[in] x posición en x del texto.
 * \param[in] y posición en y del texto.
 */
void lcdPutString(uint8_t *text, uint8_t x, uint8_t YY) {
	uint8_t i; //<? índice
	uint16_t cmd;
	cmd = (uint16_t)x +((uint16_t)YY)*LCD_COLS;
	lcdSend2DCmd(cmd , LCD_2D_SETADDRPTR); // coordenadas del puntero
	//Go torugh each char in given string and write it using auto increment
	for (i = 0; (*(text + i) != 0x00) && i < 255; i++) {
		lcdSendData( (*(text+i)-0x20) );
		lcdSendCmd(LCD_1D_WRDATAINC);
	}
	
}


/**
 * Función que envía dato al LCD pero cuando está activada la función auto write
 * \param[in] dato dato a escribir
 */
static void lcdSendAutoData(uint8_t dato){
	uint8_t i;
    if(lcdAutoWRReady())
		return;
    // Pongo todos los pines del LCD_DATO como salida
    //primero los pongo como entrada, para poner en 00 los dos bits de configuracion.
    GPIOC->MODER &= ~(0x3<<(7*2)); // GPIO_C7 como entrada 
    GPIOB->MODER &= ~(0x3<<(3*2) | 0x3<<(4*2) | 0x3<<(5*2) | 0x3<<(6*2) | 0x3<<(10*2)) ; // GPIOs B3 B5 B6 y B10 como entrada 
    GPIOA->MODER &= ~(0x3<<(8*2) | 0x3<<(9*2)) ; // GPIO_C7 como entrada 
    // para poner como salida, se pone un 01 en el moder correspondiente al pin.
    GPIOC->MODER |= (0x1<<(7*2)); // GPIO_C7 como salida
    GPIOB->MODER |= (0x1<<(3*2) | 0x1<<(4*2) | 0x1<<(5*2) | 0x1<<(6*2) | 0x1<<(10*2)) ; // GPIOs B3 B5 B6 y B10 como salida
    GPIOA->MODER |= (0x1<<(8*2) | 0x1<<(9*2)) ; // GPIO_C7 como salida
    lcdWritePort(dato);
    LCD_CD_GPIO_Port->BRR = LCD_CD_Pin;     // CD = 0
    LCD_RD_GPIO_Port->BSRR = LCD_RD_Pin; // pin RD en 1
	LCD_WR_GPIO_Port->BRR = LCD_WR_Pin; // pin WR en 0
   // for(i=0;i<48;i++){
        __NOP();    
   // }
    LCD_CE_GPIO_Port->BRR = LCD_CE_Pin; // CE = 0
//	for(i=0;i<48;i++){
        __NOP();    
  //  }
    lcdDisable();
    //primero los pongo como entrada, para poner en 00 los dos bits de configuracion.
    GPIOC->MODER &= ~(0x3<<(7*2)); // GPIO_C7 como entrada 
    GPIOB->MODER &= ~(0x3<<(3*2) | 0x3<<(4*2) | 0x3<<(5*2) | 0x3<<(6*2) | 0x3<<(10*2)) ; // GPIOs B3 B5 B6 y B10 como entrada 
    GPIOA->MODER &= ~(0x3<<(8*2) | 0x3<<(9*2)) ; // GPIO_C7 como entrada    
}


/**
 * Función para enviar un dato al lcd
 * Esta función envía un dato al lcd,
 * 
 * \param[in] dato dato a escribir 
 *  
 */
static void lcdSendData(uint8_t dato) {
//    uint8_t i;
	if (lcdReady()) {
		return;
	}
    // Pongo todos los pines del LCD_DATO como salida
    //primero los pongo como entrada, para poner en 00 los dos bits de configuracion.
    GPIOC->MODER &= ~(0x3<<(7*2)); // GPIO_C7 como entrada 
    GPIOB->MODER &= ~(0x3<<(3*2) | 0x3<<(4*2) | 0x3<<(5*2) | 0x3<<(6*2) | 0x3<<(10*2)) ; // GPIOs B3 B5 B6 y B10 como entrada 
    GPIOA->MODER &= ~(0x3<<(8*2) | 0x3<<(9*2)) ; // GPIO_C7 como entrada 
    // para poner como salida, se pone un 01 en el moder correspondiente al pin.
    GPIOC->MODER |= (0x1<<(7*2)); // GPIO_C7 como salida
    GPIOB->MODER |= (0x1<<(3*2) | 0x1<<(4*2) | 0x1<<(5*2) | 0x1<<(6*2) | 0x1<<(10*2)) ; // GPIOs B3 B5 B6 y B10 como salida
    GPIOA->MODER |= (0x1<<(8*2) | 0x1<<(9*2)) ; // GPIO_C7 como salida
	lcdWritePort(dato);
 //   for(i=0;i<48;i++){
        __NOP();    
  //  }
    LCD_RD_GPIO_Port->BSRR = LCD_RD_Pin; // pin RD en 1
    LCD_CD_GPIO_Port->BRR = LCD_CD_Pin; // pin CD en 0
	LCD_WR_GPIO_Port->BRR = LCD_WR_Pin; // pin WR en 0
    LCD_CE_GPIO_Port->BRR = LCD_CE_Pin; // CE = 0
//	for(i=0;i<48;i++){
        __NOP();    
 //   }
    lcdDisable();
    //primero los pongo como entrada, para poner en 00 los dos bits de configuracion.
    GPIOC->MODER &= ~(0x3<<(7*2)); // GPIO_C7 como entrada 
    GPIOB->MODER &= ~(0x3<<(3*2) | 0x3<<(4*2) | 0x3<<(5*2) | 0x3<<(6*2) | 0x3<<(10*2)) ; // GPIOs B3 B5 B6 y B10 como entrada 
    GPIOA->MODER &= ~(0x3<<(8*2) | 0x3<<(9*2)) ; // GPIO_C7 como entrada 
    
}


/* ----------  FUNCIONES GRAFICAS ----------------------------------------*/

/**
 *  Clears display 
 */
void lcdClearText(void) {
	uint16_t i;
	lcdSend2DCmd(0x0, LCD_2D_SETADDRPTR);
	for (i = 0; i <= LCD_COLS*LCD_ROWS; i++) {
		lcdSendData(0x00);
		lcdSendCmd(LCD_1D_WRDATAINC);
	}
	
}

/**
 * Dibuja un bitmap:
 * param[in] x posición en X si es 255-> centrar
 * param[in] y posición en Y si es 255-> centrar
 * param[in] bitmap
 * param[in] espacio	Escribe en espacio 0 o 1 de memoria.
 * Los primeros 4 bytes del bitmap deben tener la resolución en X e Y = [LSBX MSBX LSBY MSBY]
 */

void lcdDrawBitMap(uint8_t x, uint8_t y, uint8_t* bitmap, modoImpresion_t modo){
	uint8_t sizeX;		//!> Tamaño del bitmap en X
	uint8_t sizeY;		//!> Tamaño del bitmap en Y
	uint16_t actX=0;		//!> Posición actual en x del bitmap
	uint16_t actY=y;		//!> Posición actual en y del bitmap
	uint16_t index;     //!> Índice para recorrer el array del bitmap 
	uint8_t* localBitMap;	//!> Puntero hacia los datos del bitmap sin los bytes de tamaño
	uint8_t color;
	
	localBitMap = bitmap+2;
	sizeX = bitmap[0];		//tamaño del bitmap, máximo 255x255
	sizeY = bitmap[1];
	if(x==255){	//centrar en X
		x = (uint8_t)(SIZEPIX_X-sizeX)/2;
	}
	if(y==255){ // centrar en Y
		actY = y = (uint8_t) (SIZEPIX_Y-sizeY)/2; 
	}
	switch(modo){
	case ImpNormal:
		for( index=0; index<(sizeX*sizeY); index++){	//con este for recorro todo el vector
			color = (localBitMap[index/8]) & (0x1<<(index%8));	//!=0 si el bit de color es 1 y 0 si el bit de color es 0,
			lcdDrawPixel(x+actX++,actY,color);
			if(actX==sizeX){	// salta a la siguiente línea.
				actY++;
				actX = 0;		// vuelve a la columna 0
			}
		}
		break;
	case Imp180:
		actY+=sizeY;
		x += sizeX;
		for( index=0; index<(sizeX*sizeY); index++){	//con este for recorro todo el vector
			color = (localBitMap[index/8]) & (0x1<<(index%8));	//!=0 si el bit de color es 1 y 0 si el bit de color es 0,
			lcdDrawPixel(x-actX++,actY,color);
			if(actX==sizeX){	// salta a la siguiente línea.
				actY--;
				actX = 0;		// vuelve a la columna 0
			}
		}
		break;
	case Imp270:
		actY=0;
		for( index=0; index<(sizeX*sizeY); index++){	//con este for recorro todo el vector
			color = (localBitMap[index/8]) & (0x1<<(index%8));	//!=0 si el bit de color es 1 y 0 si el bit de color es 0,
			lcdDrawPixel(x+actY,y+actX++,color);
			if(actX==sizeX){	// salta a la siguiente línea.
				actY++;
				actX = 0;		// vuelve a la columna 0
			}
		}
		break;
	case Imp90:
		actY=0;
		x += sizeX;
		for( index=0; index<(sizeX*sizeY); index++){	//con este for recorro todo el vector
			color = (localBitMap[index/8]) & (0x1<<(index%8));	//!=0 si el bit de color es 1 y 0 si el bit de color es 0,
			lcdDrawPixel(x-actY,y+actX++,color);
			if(actX==sizeX){	// salta a la siguiente línea.
				actY++;
				actX = 0;		// vuelve a la columna 0
			}
		}
		break;
	case ImpFH:
		x += sizeX;
		for( index=0; index<(sizeX*sizeY); index++){	//con este for recorro todo el vector
			color = (localBitMap[index/8]) & (0x1<<(index%8));	//!=0 si el bit de color es 1 y 0 si el bit de color es 0,
			lcdDrawPixel(x-actX++,actY,color);
			if(actX==sizeX){	// salta a la siguiente línea.
				actY--;
				actX = 0;		// vuelve a la columna 0
			}
		}
		break;
	case ImpFV:
		actY+=sizeY;
		for( index=0; index<(sizeX*sizeY); index++){	//con este for recorro todo el vector
			color = (localBitMap[index/8]) & (0x1<<(index%8));	//!=0 si el bit de color es 1 y 0 si el bit de color es 0,
			lcdDrawPixel(x+actX++,actY,color);
			if(actX==sizeX){	// salta a la siguiente línea.
				actY--;
				actX = 0;		// vuelve a la columna 0
			}
		}
		break;
	}
}

/*	
 * Toma las coordenadas, las calcula en un pixel y lo rellena 
 */
void lcdDrawPixel(uint16_t x, uint16_t y, uint8_t color) {
	uint16_t direccion;
	uint8_t cmd;
	direccion = LCD_GRAPH_BASE + (x / CHAR_SIZEX) + (y * LCD_COLS);
	lcdSend2DCmd(direccion, LCD_2D_SETADDRPTR);
	// si color == 0, envía comando 11110XXX para borrar el pixel
	// si color == 1, envía comando 11111XXX para setear el pixel
	if(color!=0){
		cmd = 0xFF  + CHAR_SIZEX - 8 - (x % CHAR_SIZEX);
	}
	else{
		cmd = 0xF7  + CHAR_SIZEX - 8 - (x % CHAR_SIZEX) ;
	}
	lcdSendCmd(cmd); 
	
}


/**
 * Soobreescribe toda la memoria de la pantalla actual con 0s
 * Utiliza la función autowrite.
 * 
 */
void lcdClearGraphics(void) {
	uint16_t  i;
	lcdSend2DCmd(LCD_GRAPH_BASE, LCD_2D_SETADDRPTR);
	lcdSendCmd(LCD_SETAUTWR);
	if (lcdAutoWRReady()==0){
		for (i = 0; i < LCD_COLS*SIZEPIX_Y; i++) {
			lcdSendAutoData(0x0);
		}
	}
	else{
		Error_Handler();
	}
	lcdSendCmd(LCD_AUTORESET);
		
}

/**
 * Dibuja una línea desde X1 Y1 hasta X2 Y2.
 * \param[in] X1 inicio de la línea en x
 * \param[in] Y1 inicio de la línea en y
 * \param[in] X2 fin de la línea en x
 * \param[in] Y2 fin de la línea en y
 * \param[in] color 0 blanco 1 negro
 */
void lcdDrawLine(int16_t X1, int16_t Y1,int16_t X2,int16_t Y2, int8_t color)
{
	int16_t CurrentX;		//!> 
	int16_t CurrentY;
	int16_t Xinc;
	int16_t Yinc;
	int16_t Dx;
	int16_t Dy;
	int16_t TwoDx;
	int16_t TwoDy;
	int16_t TwoDxAccumulatedError;
	int16_t TwoDyAccumulatedError;
	Dx = (X2-X1); // dimensión horizontal.
	Dy = (Y2-Y1); // dimensión vertical.
	
	TwoDx = Dx<<1; // longitud 2*Dx
	TwoDy = Dy<<1; // longitud 2*Dy
	
	CurrentX = X1; // comienza en X1
	CurrentY = Y1; // comienza en Y1
	
	Xinc = 1; // fija el incremento horizontal
	Yinc = 1; // fija el incremento vertical
	
	if(Dx < 0) // con Dx negativo: 
	{
		Xinc = -1; // paso negativo
		Dx = -Dx;  // invierte signo Dx
		TwoDx = -TwoDx; // Dx*2
	}
	
	if (Dy < 0) // con Dy negativo
	{
		Yinc = -1; // pasos negativos
		Dy = -Dy; // 
		TwoDy = -TwoDy; // 
	}	
	
	lcdDrawPixel(X1,Y1, color); // primer pixel
	
	if ((Dx != 0) || (Dy != 0)) // comprobar si la línea se compone de mas de un punto.
	{
		if (Dy <= Dx) // almacenamiento y menor que x?
		{ 
			TwoDxAccumulatedError = 0; // resetea el error acumulado. 
			do
			{
				CurrentX += Xinc; // añadir a la etapa de la posición actual. 
				TwoDxAccumulatedError += TwoDy; // incrementa error 
				if(TwoDxAccumulatedError > Dx)  // si el error es mayor a Dx amenta current Y
				{
					CurrentY += Yinc; //
					TwoDxAccumulatedError -= TwoDx; //
				}
				lcdDrawPixel(CurrentX,CurrentY, color);// 
			}while (CurrentX != X2); //
		}
		else // 
		{
			TwoDyAccumulatedError = 0; 
			do 
			{
				CurrentY += Yinc; 
				TwoDyAccumulatedError += TwoDx;
				if(TwoDyAccumulatedError>Dy) 
				{
					CurrentX += Xinc;
					TwoDyAccumulatedError -= TwoDy;
				}
				lcdDrawPixel(CurrentX,CurrentY, color); 
			} while (CurrentY != Y2);
		}
	}
	
}

